# Bitbucket Pipelines Pipe: 42Crunch REST API Static Security Testing

The REST API Static Security Testing pipe lets you add an automatic static application security testing (SAST) task to your CI/CD pipelines. The pipe checks your OpenAPI files for their quality and security from a simple Git push to your project repository when the CI/CD pipeline runs.

The pipe is powered by 42Crunch [API Security Audit](https://docs.42crunch.com/latest/content/concepts/api_contract_security_audit.htm). Security Audit performs a static analysis of the API definition that includes more than 300+ checks on best practices and potential vulnerabilities on how the API defines authentication, authorization, transport, and data coming in and going out. For more details on the checks, see [API Security Encyclopedia](https://apisecurity.io/encyclopedia/content/api-security-encyclopedia.htm).

As a result of the security testing, your APIs get an audit score, with 100 points meaning the most secure, best defined API. By default, the threshold score for the pipe to pass is 75 points for each audited API, but you can change the minimum score in the settings of the pipe.

API contracts must follow the OpenAPI Specification (OAS) (formely Swagger). Both OAS v2 and v3, and both JSON and YAML formats are supported.

### Discover APIs

By default, the pipe locates all OpenAPI files in your project and submits them for static security testing. You can include or exclude specific paths from the discovery phase and can omit the discovery phase completely by adding a pipe configuration file `42c-conf.yaml` in the root of your repository and specifying rules for the discovery phase. For more details, see the [documentation](https://docs.42crunch.com/latest/content/tasks/integrate_bitbucket_pipelines.htm).

All discovered APIs are uploaded to an API collection in 42Crunch Platform. The pipe uses the environment variables `BITBUCKET_REPO_FULL_NAME` and `BITBUCKET_BRANCH`/`BITBUCKET_TAG`/`BITBUCKET_PR_ID` to show the repository and the branch/tag/PR name from where the API collection originated from. During the subsequent pipe runs, the APIs in the collection are kept in sync with the changes in your repository.

### Fine-tune the pipe

You can add a task configuration file `42c-conf.yaml` in the root of your repository, and to fine-tune the success/failure criteria. For example, you can choose on whether to accept invalid API contracts, or define a cut-off on a certain level of issue severity.

You can define different task configuration for different branches/tags/PRs in your repository.

You can specify the full name of the branch (for example, `master`) or a wildcard (for example, `release-*`).

For more details, see the [documentation](https://docs.42crunch.com/latest/content/tasks/integrate_bitbucket_pipelines.htm).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: 42crunch/api-security-audit:3.5.0
```

## Variables

| Variable                | Usage                                                                                                                                                                     |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| API_TOKEN (\*)          | The API token to access 42Crunch Platform. Default: $\{SECURED_42C_API_TOKEN\} variable.                                                                                  |
| MIN_SCORE               | Minimum score for OpenAPI files. Default: 75                                                                                                                              |
| PLATFORM_URL            | 42Crunch Platform URL. Default: https://us.42crunch.cloud
| DEFAULT_COLLECTION_NAME | The default collection name used when creating collections for discovered apis.                                                                                           |
| ROOT_DIRECTORY          | The root directory that contains the 42c-conf.yaml configuration file and the APIs.                                                                                       |
| LOG_LEVEL               | Log level, one of FATAL, ERROR, WARN, INFO, DEBUG. Default: INFO                                                                                                          |
| SHARE_EVERYONE          | Share new API collections with everyone, one of: OFF, READ_ONLY, READ_WRITE. Default: OFF                                                                                 |
| JSON_REPORT             | Writes Audit report in JSON format to a specified file, optional. Default: undefined, no report is written                                                                |
| API_TAGS                | Set tags for newly created APIs in the format “category1:name1 category2:name2”, optional.                                                                                |
| SKIP_LOCAL_CHECKS       | If set to 'true', disables all local failure conditions (like minimum score) and fails execution only if the criteria defined in SQGs are not met. Default is false.      |
| IGNORE_FAILURES         | If set to 'true', forces to complete execution successfully even if the failures conditions (like min-score or SQG criteria) you have set are met. Default is false.      |
| IGNORE_NETWORK_ERRORS   | If set to 'true', forces to complete execution successfully even if a network error has occurred (such as a failure to connect to 4unch Platform, etc). Default is false. |

_(\*) = required variable._

## Prerequisites

Create an API token in 42Crunch platform and copy its value into a secured repository variable named `SECURED_42C_API_TOKEN`.

For more details, see the [full documentation](https://docs.42crunch.com/latest/content/tasks/integrate_bitbucket_pipelines.htm).

## Examples

Basic example:

```yaml
script:
  - pipe: 42crunch/api-security-audit:3.5.0
```

Advanced example:

```yaml
script:
  - pipe: 42crunch/api-security-audit:3.5.0
    variables:
      MIN_SCORE: "85"
      PLATFORM_URL: "https://us.42crunch.cloud"
      LOG_LEVEL: "DEBUG"
```

## Support

The pipe is maintained by support@42crunch.com. If you run into an issue, or have a question not answered here, you can create a support ticket at [support.42crunch.com](https://support.42crunch.com/).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
