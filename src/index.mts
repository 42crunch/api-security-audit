/*
 Copyright (c) 42Crunch Ltd. All rights reserved.
 Licensed under the GNU Affero General Public License version 3. See LICENSE.txt in the project root for license information.
*/

import { resolve } from "path";
import { bootstrap } from "global-agent";
import { audit } from "@xliic/cicd-core-node";
import {
  submitReport,
  submitAnnotations,
  enableProxy,
} from "./codeinsight.mjs";
import { produceReport, produceAnnotations } from "./report.mjs";
import { Logger, Reference, SharingType } from "@xliic/cicd-core-node";

const ignoreFailures = process.env["IGNORE_FAILURES"] === "true";

function getenv(names: string[]): any {
  const result: any = {};
  for (const name of names) {
    if (!(name in process.env)) {
      console.error(`Environment variable ${name} is not set, exiting.`);
      process.exit(-1);
    }
    result[name] = process.env[name];
  }
  return result;
}

function logger(levelName: string): Logger {
  const levels = {
    FATAL: 5,
    ERROR: 4,
    WARN: 3,
    INFO: 2,
    DEBUG: 1,
  };

  const level = levels[levelName.toUpperCase()] ?? levels.INFO;

  return {
    debug: (message: string) => {
      if (levels.DEBUG >= level) {
        console.log(message);
      }
    },
    info: (message: string) => {
      if (levels.INFO >= level) {
        console.log(message);
      }
    },
    warning: (message: string) => {
      if (levels.WARN >= level) {
        console.log(message);
      }
    },
    error: (message: string) => {
      if (levels.ERROR >= level) {
        console.log(message);
      }
    },
    fatal: (message: string) => {
      if (levels.FATAL >= level) {
        console.log(message);
      }
    },
  };
}

function getInputValue(input: string, options: any, defaultValue: any): any {
  if (!(input in process.env)) {
    return defaultValue;
  }

  const value = process.env[input];
  if (options.hasOwnProperty(value)) {
    return options[value];
  }

  console.log(
    `Unexpected value for variable "${input}" using default value instead`
  );

  return defaultValue;
}

function getReference(): Reference | undefined {
  const branch = process.env["BITBUCKET_BRANCH"];
  if (branch !== undefined && branch !== "") {
    return { branch };
  }

  const tag = process.env["BITBUCKET_TAG"];
  if (tag !== undefined && tag !== "") {
    return { tag };
  }

  const pr = process.env["BITBUCKET_PR_ID"];
  const targetBranch = process.env["BITBUCKET_PR_DESTINATION_BRANCH"];
  if (
    pr !== undefined &&
    pr !== "" &&
    targetBranch !== undefined &&
    targetBranch !== ""
  ) {
    return { pr: { id: pr, target: targetBranch } };
  }
}

async function run() {
  const {
    BITBUCKET_WORKSPACE,
    BITBUCKET_REPO_SLUG,
    BITBUCKET_COMMIT,
    BITBUCKET_REPO_FULL_NAME,
    API_TOKEN,
    MIN_SCORE,
  } = getenv([
    "BITBUCKET_WORKSPACE",
    "BITBUCKET_REPO_SLUG",
    "BITBUCKET_COMMIT",
    "BITBUCKET_REPO_FULL_NAME",
    "API_TOKEN",
    "MIN_SCORE",
  ]);

  const repositoryUrl = `https://bitbucket.org/${BITBUCKET_REPO_FULL_NAME}`;
  const userAgent = "CICD-Bitbucket/2.0";
  const onboardingUrl =
    "https://docs.42crunch.com/latest/content/tasks/integrate_bitbucket_pipelines.htm";

  const platformUrl =
    "PLATFORM_URL" in process.env
      ? process.env["PLATFORM_URL"]
      : "https://us.42crunch.cloud";

  const logLevel =
    "LOG_LEVEL" in process.env ? process.env["LOG_LEVEL"] : "INFO";

  const shareEveryone = getInputValue(
    "SHARE_EVERYONE",
    {
      OFF: undefined,
      READ_ONLY: SharingType.ReadOnly,
      READ_WRITE: SharingType.ReadWrite,
    },
    undefined
  );

  const rootDir = resolve(process.cwd(), process.env["ROOT_DIRECTORY"] || ".");
  const defaultCollectionName = process.env["DEFAULT_COLLECTION_NAME"];
  const writeJsonReportTo = process.env["JSON_REPORT"];
  const api_tags = process.env["API_TAGS"];
  const skipLocalChecks = process.env["SKIP_LOCAL_CHECKS"] === "true";
  const ignoreNetworkErrors = process.env["IGNORE_NETWORK_ERRORS"] === "true";

  const reference = getReference();
  if (!reference) {
    console.error("Unable to retrieve the branch/tag or PR name");
    process.exit(-1);
  }

  if (ignoreFailures) {
    console.log("Ignoring security audit failures");
  }
  if (ignoreNetworkErrors) {
    console.log("Ignoring network errors");
  }

  try {
    const result = await audit({
      rootDir,
      referer: repositoryUrl,
      userAgent,
      apiToken: API_TOKEN,
      onboardingUrl,
      platformUrl,
      logger: logger(logLevel),
      minScore: MIN_SCORE,
      repoName: repositoryUrl,
      reference: reference,
      cicdName: "bitbucket",
      shareEveryone,
      defaultCollectionName,
      writeJsonReportTo,
      api_tags,
      skipLocalChecks,
    });

    const report = produceReport(result, platformUrl);
    const annotations = produceAnnotations(result.files, platformUrl);

    enableProxy();

    await submitReport(
      report,
      BITBUCKET_WORKSPACE,
      BITBUCKET_REPO_SLUG,
      BITBUCKET_COMMIT
    );

    await submitAnnotations(
      annotations,
      BITBUCKET_WORKSPACE,
      BITBUCKET_REPO_SLUG,
      BITBUCKET_COMMIT
    );

    return result;
  } catch (ex) {
    console.log(
      "Error: ",
      ex.message,
      ex?.code || "",
      ex?.response?.body || ""
    );
    if (ignoreNetworkErrors && ex?.isNetworkError) {
      process.exit(0);
    } else {
      process.exit(-1);
    }
  }
}

bootstrap();

(async () => {
  const result = await run();
  if (!ignoreFailures) {
    if (result.failures > 0 || result.files.size == 0) {
      // exit with -1 if any failures are found or no api files checked
      process.exit(-1);
    }
  }
})();
