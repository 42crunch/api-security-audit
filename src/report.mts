/*
 Copyright (c) 42Crunch Ltd. All rights reserved.
 Licensed under the GNU Affero General Public License version 3. See LICENSE.txt in the project root for license information.
*/

import { AuditResult, FileAuditMap } from "@xliic/cicd-core-node";

export function produceAnnotations(
  summary: FileAuditMap,
  platformUrl: string
): any {
  const annotations = [];
  const keys = Array.from(summary.keys());
  for (let i = 0; i < keys.length; i++) {
    const filename = keys[i];
    const result = summary.get(filename);
    if ("errors" in result) {
      // skip error result
      continue;
    }
    /* ignore individual issues for now
    if (result.issues.length > 0) {
      for (let j = 0; j < result.issues.length; j++) {
        const issue = result.issues[j];
        const annotation: any = {
          external_id: `issue-${i}-${j}`,
          annotation_type: "VULNERABILITY",
          summary: issue.description,
          path: path.relative(process.cwd(), issue.file),
          line: issue.line,
          severity: issue.severity,
        };

        if (result.apiId) {
          annotation.link = `${PLATFORM_URL}/apis/${result.apiId}/security-audit-report`;
        }

        annotations.push(annotation);
      }
    }
    */
    if (result.failures.length > 0) {
      for (let j = 0; j < result.failures.length; j++) {
        const annotation: any = {
          external_id: `failure-${i}-${j}`,
          annotation_type: "VULNERABILITY",
          result: "FAILED",
          summary: result.failures[j].substring(0, 450), // max summary size is 450 chars
          path: filename,
          line: 0,
          //severity: "CRITICAL",
        };

        if (result.id) {
          annotation.link = `${platformUrl}/apis/${result.id}/security-audit-report`;
        }

        annotations.push(annotation);
      }
    } else {
      annotations.push({
        external_id: `success-${i}`,
        annotation_type: "VULNERABILITY",
        result: "PASSED",
        summary: "No issues found",
        path: filename,
        line: 0,
      });
    }
  }
  return annotations;
}

export function produceReport(result: AuditResult, platformUrl: string): any {
  const report = {
    title: "42Crunch REST API Static Security Testing",
    report_type: "SECURITY",
    reporter: "42Crunch",
    link: platformUrl,
  };

  if (result.failures > 0) {
    return {
      ...report,
      result: "FAILED",
      details: `Detected ${result.failures} failure(s) in the ${result.files.size} OpenAPI file(s) checked`,
    };
  } else if (result.files.size === 0) {
    return { ...report, result: "FAILED", details: "No OpenAPI files found" };
  } else {
    return { ...report, result: "PASSED", details: "No issues found" };
  }
}
