/*
 Copyright (c) 42Crunch Ltd. All rights reserved.
 Licensed under the GNU Affero General Public License version 3. See LICENSE.txt in the project root for license information.
*/

import got from "got";

const externalId = "42crunch-api-audit";

export function enableProxy() {
  (global as any).GLOBAL_AGENT.HTTP_PROXY = "http://host.docker.internal:29418";
}

export async function submitReport(
  report: any,
  workspace: string,
  repo: string,
  commit: string
) {
  const url = `http://api.bitbucket.org/2.0/repositories/${workspace}/${repo}/commit/${commit}/reports/${externalId}`;

  await got.delete(url, { throwHttpErrors: false });

  await got.put(url, {
    json: report,
    responseType: "json",
  });
}

export async function submitAnnotations(
  annotations: any,
  workspace: string,
  repo: string,
  commit: string
) {
  const url = `http://api.bitbucket.org/2.0/repositories/${workspace}/${repo}/commit/${commit}/reports/${externalId}/annotations`;

  const chunkSize = 100;
  for (let i = 0; i < annotations.length; i += chunkSize) {
    const chunk = annotations.slice(i, i + chunkSize);
    await got.post(url, {
      json: chunk,
      responseType: "json",
    });
  }

  /* use bulk uploads 
  for (const annotation of annotations) {
    // only submit critical or high ones, or results, looks like bitbucket UI has issues with paging
    if (
      annotation.result ||
      annotation.severity == "CRITICAL" ||

      annotation.severity == "HIGH"
    ) {
      console.log("submit annotation", annotation);
      const result = await got.put(`${url}/${annotation.external_id}`, {
        json: annotation,
        responseType: "json",
      });
      console.log("response", result.statusCode);
    }
  }
  */
}
