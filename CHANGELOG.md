# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.5.0

- minor: Update to a newer version of cicd-core and typescript

## 3.4.0

- minor: Extend JSON Report with discovery collection information

## 3.3.0

- minor: Add new parameter IGNORE_FAILURES
- minor: Add new parameter IGNORE_NETWORK_ERRORS
- minor: Add new parameter JSON_REPORT
- minor: Add new parameter SKIP_LOCAL_CHECKS

## 3.2.0

- minor: Add support for checking organization specific naming conventions for collection and API names

## 3.1.0

- minor: Update dependencies and base node version

## 3.0.0

- major: Update to newest version of cicd-core adding support for PRs, tags, security quality gates, etc

## 2.0.1

- patch: Limit summary length in the report

## 2.0.0

- major: Implement repository and branch based collection management. Introduce new 42c-conf.yaml format with support for branches

## 1.0.5

- patch: Improve readme text and fix pipe metadata.

## 1.0.4

- patch: Improve testsuite

## 1.0.3

- patch: Fix release script issues

## 1.0.2

- patch: Rename repository, republish docker image

## 1.0.1

- patch: Add icon.

## 1.0.0

- major: Release API Security Audit pipe
