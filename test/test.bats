#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/api-security-audit"}

  echo "Building image..."
  docker build --target builder -t ${DOCKER_IMAGE}:test .
}

@test "AVA tests" {
    run docker run --env API_TOKEN ${DOCKER_IMAGE}:test npm test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

