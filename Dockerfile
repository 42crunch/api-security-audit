FROM node:18.16-alpine3.17 AS builder

RUN mkdir /pipe
COPY . /pipe
WORKDIR /pipe
RUN mkdir dist && npm ci && npm run build

FROM node:18.16-alpine3.17

ENV NODE_ENV=production
RUN apk add --no-cache tini
RUN mkdir /pipe && chown node:node /pipe
USER node
COPY package*.json /pipe/
COPY --from=builder /pipe/dist /pipe/dist/
WORKDIR /pipe
RUN npm ci --production
WORKDIR /
ENTRYPOINT [ "/sbin/tini", "--", "node", "/pipe/dist/index.mjs" ]
